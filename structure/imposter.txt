Imposter

How do you introduce a new variation into a computation? Introduce a new object with the same protocol as an existing object but a different implementation.

Introducing variation in a procedural program involves adding conditional logic. Such logic tends to proliferate and a healthy does of polymorphic messages are required to cure the duplication.

Suppose you have a structure in place already. There's an object already. Now you need the system to do something different. If there's an obvious place to insert an if statement and you're not duplicating logic from elsewhere, go ahead. Often however, the variation would obviously require changes to several methods.

This moment of decision comes up in two ways in TDD. Sometimes you are writing a test case and you need to represent a new scenario. None of the existing objects expresses what you want to express. 

Generally, spotting the possibility of an Imposter the first time requires insight.

Following are two examples of Imposters that come up during refactoring:

- Null Object - you can treat the absence of data the same as the presence of data.
- Composite - you can treat a collecton of objects the same as a single object.

Finding imposters during refactoring is driven by eliminating duplication, just as all refactoring is driven by eliminating duplication.
