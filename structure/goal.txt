Goal
===============

1. The goal is to reduce the defect density of code and make the subject of work crystal clear to all involved. 

2. The goal is to write clean code that works. This is out of reach for most programmers most of the time. Divide and conquer. First we'll solve the 'that works' part of the problem. Then we'll solve the 'clean code' part. You learn as you solve the 'that works' problem.

Imagine a world where the code is clear and direct with no complicated solutions, only complicated problems begging for careful thought. TDD is a practice that can help you lead yourself to that careful thought. 

The goal is clean code that works and for a lot of reasons:

- is a predictable way to develop. You know when you are finished, without having to worry about a long bug trail

- gives you a chance to learn all the lessons that the code has to teach you. If you only slap together the first thing you think of, you never have time to think of a second, better, thing.

- improves the lives of user of our software
- lets your teammates count on you, and you on them.
- feels good

===============
How do you get to clean code that works?
===============

Drive development with automated tests, a style of development called Test Driven Development.

===============
Two Simple Rules
===============

In TDD, you :
 - Write new code only if you first have a failing test.
 - Eliminate duplication
 
Two simple rules, but they generate complex individual and group behavior

Some of the technical implications are :

- You must design organically, with running code providing feedback between decisions. 
- You must write your own tests, since you can't wait 20 times a day for someone else to write a test.
- Your development environment must provide rapid response to small changes.
- Your designs must consist of many highly cohesive, loosely coupled components just to make testing easy.

The two rules imply an order to the tasks of programming:

red-green-refactor.txt

Following these two simple rules can lead us to work much closer to our potential:

1. Write a failing automated test before you write any code.
2. Remove duplication.

How exactly to do this, the subtle gradations in applying these rules and the lengths to which you can push these two simple rules are the topic of this book.

Why rules #2? Answer is in dependency-duplication.txt