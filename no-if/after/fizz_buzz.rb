require_relative 'fizz_buzz_engine'
require_relative 'fixnum_extensions'

class FizzBuzz
  using FixnumExtensions
  
  def numbers
    (1..100).to_a
  end  
  
  # Uses Refinement
  def sequence
    numbers.collect do |x|
      x.fizz_buzz
    end
  end

  # No if constraint
  # def sequence
  #   numbers.collect do |x|
  #     fbe = FizzBuzzEngine.new(x)
  #     fbe.value
  #   end
  # end
  
  #  No if constraint
  # private
  #
  # def multiple_of(n, x)
  #   x.modulo(n).zero?
  # end
end