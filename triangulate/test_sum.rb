require 'minitest/autorun'

class Summer
  def sum(list)
    result = 0
    list.each do |element|
      result += element
    end
    result
  end  
end

class TestSum < MiniTest::Test
  def test_sum_list_of_numbers_with_0_elements
    summer = Summer.new
    
    result = summer.sum([])
    
    assert_equal 0, result
  end
  
  def test_sum_list_of_numbers_with_1_element
    summer = Summer.new
    
    result = summer.sum([1])
    
    assert_equal 1, result    
  end
  
  def test_sum_list_of_numbers_with_2_elements
    summer = Summer.new
    
    result = summer.sum([1,2])
    
    assert_equal 3, result        
  end  
end