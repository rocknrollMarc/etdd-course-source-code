require 'minitest/autorun'

class Calculator
  def add(augend, addend)
    augend + addend
  end
end

class TestCalculator < MiniTest::Test
  def test_addition
    calculator = Calculator.new
    
    result = calculator.add(1,2)
    
    assert_equal 3, result
  end
  
end